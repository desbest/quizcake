# Quizcake

Make multiple choice quizzes and results of your answers, with teams. This was made for revision purposes when I was at university.

Quizcake allows you to make multiple choice quizzes, and you are given your answers when you complete the quiz. There's also charts when given the answers. And you can compete in teams.

Copyright desbest 2011-2020

![quizcake home page](https://i.imgur.com/SaUmA8G.png)

![quizcake quiz page](https://i.imgur.com/DiIKbRe.png)

![quizcake results page](https://i.imgur.com/2PGrTwz.png)