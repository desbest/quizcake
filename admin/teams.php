<?php
require ("../config.php");

if (isset($_POST['addteam']) && $_POST['addteam'] && $logged['level'] == 5){
    $teamname = $_POST[teamname];
    $countteam = mysql_query("SELECT * FROM teams WHERE teamname='$teamname'");
    $countteam = mysql_num_rows($countteam);
    if ($countteam == 0){
        mysql_query("INSERT INTO teams 
        (teamname) VALUES('$teamname' ) ") 
        or die(mysql_error());
        $notice = "You have added a team!";
    }
}

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Quizcake</title>
<link rel="stylesheet" type="text/css" href="../resources/1kbgrid.css">
<link rel="stylesheet" type="text/css" href="../resources/stylesheet.css">
</head>
<body><div class="container">

<div class="row">
    <div class="column grid_12"><font class="header">Quizcake Admin Panel</font></div>
</div>
<div class="row">
    <div class="column grid_3">
    
        <div class="header">Quizcake</div>
        <div id="navcontainer">
        <ul id="navlist">
        <?php
        $fetchquizzes = mysql_query("SELECT * FROM quizzes ORDER BY quizname ASC");
        while ($quiz = mysql_fetch_array($fetchquizzes)){
            if (isset($_GET['id']) && $_GET['id'] == $quiz['id']){
            echo "<li id=\"active\"><a href=\"quiz.php?id=$quiz[id]\">$quiz[quizname]</a></li>";
            } else {
            echo "<li><a href=\"quiz.php?id=$quiz[id]\">$quiz[quizname]</a></li>";
            }
        }
        ?>
        <li></li>
        <li><a href="quiz.php">New Quiz</a></li>
        </ul>
        </div>
        
        <div class="header">Miscellaneous</div>
        <div id="navcontainer">
        <ul id="navlist">
        <li id="active"><a href="teams.php">Teams</a></li>
        <li><a href="index.php">Statistics</a></li>
        </ul>
        </div>

        
    </div>
    <div class="column grid_8">
    
        <?php
        if (isset($notice)){
        echo "<div class=\"notice\">
        $notice
        </div>"; 
        }
        ?>
    
        <?php if (!isset($logged['id'])){
        echo "<div class=\"header\"><h1>Authentication Required</h1></div>
        You have to be logged in to view this page.<br><br><a href=\"index.php\">Go to login</a>";
        
        }
        if (isset($logged['level']) && $logged['level'] == 5){
        ?>
        <div class="header"><h1>Teams</h1></div>
        <?php
        $fetchteams = mysql_query("SELECT * FROM teams ORDER BY teamname ASC");
        while ($team = mysql_fetch_array($fetchteams)){
            echo "$team[teamname]<br>";
        }
        echo "<br>";
        ?>
        Add new team
        <br><form method="POST">
            <input type="text" name="teamname" class="bigtext">
            <input type="submit" name="addteam" class="bigbutton" value="Add team">
        </form>

    <?php } ?></div>
</div>

</div></body>
</html>