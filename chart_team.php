<?php
include_once( 'config.php' );
include_once( 'resources/openflashchart1/open-flash-chart.php' );

    //bar chart
    $bar = new bar_outline( 50, '#9933CC', '#8010A0' );
        
    $bar->key( 'Percentage correct', 10 );

if ($_GET[quiz] == ""){
    $sql = mysql_query("SELECT *, AVG(percentage) FROM avgscores GROUP BY teamid");
    $min = mysql_query("SELECT *, MIN(percentage) FROM avgscores GROUP BY teamid"); 
    $max = mysql_query("SELECT *, AVG(percentage) FROM avgscores GROUP BY teamid"); 
} else {
    $sql = mysql_query("SELECT *, AVG(percentage) FROM avgscores WHERE quizid='$_GET[quiz]' GROUP BY teamid");
    $min = mysql_query("SELECT *, MIN(percentage) FROM avgscores WHERE quizid='$_GET[quiz]' GROUP BY teamid"); 
    $max = mysql_query("SELECT *, AVG(percentage) FROM avgscores WHERE quizid='$_GET[quiz]' GROUP BY teamid"); 
}

$min = mysql_fetch_array($min);
$max = mysql_fetch_array($max);
$min2 = $min['MIN(teamid)'];

$min = 0;

$data = array();

//for pie and line chart types
/* while( $row = mysql_fetch_array($sql) )
{
    
  $data[] = ceil($row['AVG(percentage)']);
    $team = mysql_query("SELECT * FROM teams WHERE id='$row[teamid]'");
    $team = mysql_fetch_array($team);
  $slimy[] = $team['teamname'];
} */

//for bar charts
while( $row = mysql_fetch_array($sql) ) {
    $bar->data[] = ceil($row['AVG(percentage)']);
            $team = mysql_query("SELECT * FROM teams WHERE id='$row[teamid]'");
            $team = mysql_fetch_array($team);
    $slimy[] = $team['teamname'];
}

/*  //line and pie charts
$max2 = max($data);
$min2 = min($data);
*/
    //pie charts
$max2 = 100; $min2 = 0;


// use the chart class to build the chart:

$g = new graph();

    //bar chart
    $g->data_sets[] = $bar;

if ($_GET[quiz] == ""){
    $g->title( 'Overall', '{font-size: 20px; padding-bottom: 8px;}' );
} else {
    $quiz = mysql_query("SELECT * FROM quizzes WHERE id='$_GET[quiz]'");
    $quiz = mysql_fetch_array($quiz);
    $g->title( "$quiz[quizname]", '{font-size: 20px; padding-bottom: 8px;}' );
}

$g->set_data( $data );

$g->set_x_labels( $slimy );
$g->set_y_min( $min2 );
$g->set_y_max( $max2 );
$g->y_label_steps( 3 );

$g->set_y_legend( 'Percentage correct', 14, '#639F45' );
$g->set_tool_tip( 'Percentage correct:<br>#val#%' );

/* //pie chart
$g->pie(60,'#505050','{font-size: 12px; color: #404040;}');
$g->pie_values($data, $slimy);
$g->pie_slice_colours( array('#d01f3c','#356aa0','#C79810') );  */

//line chart
//$g->line_dot( 3, 5, '0xCC3399', 'Percentage correct', 10);    // <-- 3px thick + dots    

// display the data
echo $g->render();
?>