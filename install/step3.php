<html>
<head>
<title>Quizcake Installer</title>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#loading").hide();    
   $("#hideonclick").click(function (){
         $("#hideonclick").hide();    
         $("#loading").show();    
   });
});
</script>
</head>
<body>

<div id="navcontainer">
<ul id="navlist">
<li><a>Step 1</a></li>
<li><a>Step 2</a></li>
<li id="active"><a id="current">Step 3</a></li>
<li><a>Step 4</a></li>
</ul>
</div>

<div class="offset">
<font class="header">Install Quizcake</font>
<?php
// ob_start();
// allows you to use cookies
require ("../config.php");
//gets the config page
if (isset($_POST['register']))
{
    // the above line checks to see if the html form has been submitted
    if (!isset($conn)) 
    {
        //checks to make sure no fields were left blank
        echo "<img src=\"warning.png\">Your database configuration was incorrect.
		<br><a href=\"step2.php\">Go back</a>
		";
    }
    else
    {
        //none were left blank!  We continue...
        

$sql = array (
           0 => "
           -- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 07, 2020 at 02:33 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';


--
-- Database: `quizcake`
--



         ",
         1 => " 
-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `questionid` int(11) NOT NULL,
  `answertitle` varchar(255) CHARACTER SET utf8 NOT NULL,
  `correct` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

       ",
         2 => " 
-- --------------------------------------------------------

--
-- Table structure for table `avgscores`
--

CREATE TABLE `avgscores` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `quizid` int(11) NOT NULL,
  `teamid` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `completes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

         ",
         3 => " 



-- --------------------------------------------------------

--
-- Table structure for table `faillogs`
--

CREATE TABLE `faillogs` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `datenumber` varchar(11) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


         ",
         4 => "

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `quizid` int(11) NOT NULL,
  `questiontitle` varchar(255) CHARACTER SET utf8 NOT NULL,
  `answers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
           ",
          5 => " 

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

CREATE TABLE `quizzes` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `quizname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `questions` int(11) NOT NULL,
  `questionstoask` int(11) NOT NULL,
  `completes` int(11) NOT NULL,
  `datenumber` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ",
         6 => "
-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `teamname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `completes` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


         ",
         7 => "
              --
-- Table structure for table `users`
--


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `password` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `level` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


         "
         );




$dbhost = '';
@set_time_limit(0);
// $db = mysql_connect($dbhost, $dbusername, $dbpassword);
// if(!$db) die('Cannot connect: ' . mysql_error());
// $res = mysql_select_db($database);
// if(!$res) die('Cannot select database "' . $database . '": ' . mysql_error());
for($i=0; $i<count($sql); $i++)
{
 // if($table_prefix !== 'phpbb_') $sql[$i] = preg_replace('/phpbb_/', $table_prefix, $sql[$i]);
 $res = mysql_query($sql[$i]);
 if(!$res) { echo '<hr><b>error in query ', ($i + 1), ': </b>', mysql_error(), ''; echo "<br><textarea cols=\"50\" class=\"mediumtext\" rows=\"16\">$sql[$i]</textarea>"; }
}
echo ("
<script type=\"text/javascript\">
   $(document).ready(function(){
      $(\"#loading\").hide();    
   });
</script>

<br><br>
");
         if ($res) {
         echo "<img src=\"tick.png\" align=\"left\">The database has been populated.
         <br><a href=\"step4.php\">Continue to step 5</a>
         "; }if (!$res) {
         echo "<img src=\"warning.png\" align=\"left\">There were errors!
         <br><a href=\"http://desbest.com\">Contact desbest from his website.</a>
         <br><a href=\"step4.php\">Continue anyway.</a>
         ";
         }
//echo 'done (', count($sql), ' queries).';

		
				
            }
        }
    
	
else
{
    // the form has not been submitted...so now we display it.
    
include ("../config.php");
    $tryme = mysql_query("SELECT * FROM users");  
    $tryme2 = mysql_query("SELECT * FROM items");  
    if ($tryme ||$tryme2){
    echo "<br><img src=\"warning.png\" align=\"left\">Stock Manager has been already installed.
    <br>Uninstall it before installing again by deleting the mysql database.
    <br><br>
    <br>If you are looking to upgrade Stock Control to a newer version, disregard that warning.
    <br>You should instead, <a href=\"step4upgrade.php\">choose the upgrade option.</a>
    ";
    } else{


    echo ("
<br>Click the button below ONCE.

<form method=\"POST\" >



<br>
<input name=\"register\" type=\"submit\" id=\"hideonclick\"class=\"bigbutton\" value=\"Populate Database Tables\">
<div id=\"loading\"><img src=\"loading.gif\"></div>
</form>
			
 "); 
 }
}






			
?>			  
			  



</div>			  
</body>
</html>